<?php 

class Receta{

	public $id_receta;
	public $nombre;
	public $ingredientes;
	public $descripcion;
	public $url;
	public $imagen;

	public function __construct($registro){

		$this->id_receta=$registro['id_receta'];
		$this->nombre=$registro['nombre'];
		$this->ingredientes=$registro['ingredientes'];
		$this->descripcion=$registro['descripcion'];
		$this->url=$registro['url'];
		$this->imagen=$registro['imagen'];
		$this->conexion=Conexion::conectar();
	}
}

?>