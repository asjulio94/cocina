<?php

class Recetas{

	private $conexion;
	public $recetas;

	public function __construct(){
		$this->recetas=[];
		$this->conexion=Conexion::conectar();
	}

	public function detalle($id){
		$sql="SELECT * FROM recetas WHERE id_receta=$id";
		$consulta=$this->conexion->query($sql);
		$registro=$consulta->fetch_assoc();
		$receta=new Receta($registro);
		return $receta;
	}

	public function detallesalto($receta){

		$id_receta = $receta['id_receta'];
		$nombre = $receta['nombre'];
		$ingredientes = explode('<br>', $receta['ingredientes']);
		$receta['ingredientes'] = $ingredientes;
		$descripcion = explode('<br>', $receta['descripcion']);
		$receta['descripcion'] = $descripcion;
		$url = $receta['url'];
		$imagen = $receta['imagen'];
		return $receta;
	}

	public function listado(){
		$sql="SELECT * FROM recetas";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas; //Array de recetas
	}

	public function aperitivos(){
		$sql="SELECT *
		FROM recetas, categorias, tipo
		WHERE categorias.id_categoria = tipo.id_categoria
		AND tipo.id_receta = recetas.id_receta
		AND categorias.cnombre LIKE 'Aperitivos'";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;

	}

	public function Pplatos(){
		$sql="SELECT *
		FROM recetas, categorias, tipo
		WHERE categorias.id_categoria = tipo.id_categoria
		AND tipo.id_receta = recetas.id_receta
		AND categorias.cnombre LIKE 'Primeros platos'";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;

	}

	public function Splatos(){
		$sql="SELECT *
		FROM recetas, categorias, tipo
		WHERE categorias.id_categoria = tipo.id_categoria
		AND tipo.id_receta = recetas.id_receta
		AND categorias.cnombre LIKE 'Segundos Platos'";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;

	}

	public function VegetarianasE(){
		$sql="SELECT *
		FROM recetas
		WHERE recetas.id_receta = 11
		OR recetas.id_receta = 16
		OR recetas.id_receta = 20
		OR recetas.id_receta = 21
		OR recetas.id_receta = 34
		OR recetas.id_receta = 37
		OR recetas.id_receta = 38";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;

	}

	public function VegetarianasA(){
		$sql="SELECT *
		FROM recetas
		WHERE recetas.id_receta = 7
		OR recetas.id_receta = 10";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;

	}

	public function VegetarianasV(){
		$sql="SELECT *
		FROM recetas
		WHERE recetas.id_receta = 23
		OR recetas.id_receta = 24
		OR recetas.id_receta = 25
		OR recetas.id_receta = 26
		OR recetas.id_receta = 27
		OR recetas.id_receta = 28
		OR recetas.id_receta = 29
		OR recetas.id_receta = 31
		OR recetas.id_receta = 32";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;

	}

	public function VegetarianasS(){
		$sql="SELECT *
		FROM recetas
		WHERE recetas.id_receta = 57";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;

	}

	public function VegetarianasP(){
		$sql="SELECT *
		FROM recetas
		WHERE recetas.id_receta = 17
		OR recetas.id_receta = 54";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;

	}

	public function VegetarianasL(){
		$sql="SELECT *
		FROM recetas
		WHERE recetas.id_receta = 12
		OR recetas.id_receta = 13
		OR recetas.id_receta = 14
		OR recetas.id_receta = 15";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;

	}

	public function Salsas(){
		$sql="SELECT *
		FROM recetas, categorias, tipo
		WHERE categorias.id_categoria = tipo.id_categoria
		AND tipo.id_receta = recetas.id_receta
		AND categorias.cnombre LIKE 'Salsas'";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;

	}

		public function Masas(){
		$sql="SELECT *
		FROM recetas, categorias, tipo
		WHERE categorias.id_categoria = tipo.id_categoria
		AND tipo.id_receta = recetas.id_receta
		AND categorias.cnombre LIKE 'Masas'";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;
	}

	public function Postres(){
		$sql="SELECT *
		FROM recetas, categorias, tipo
		WHERE categorias.id_categoria = tipo.id_categoria
		AND tipo.id_receta = recetas.id_receta
		AND categorias.cnombre LIKE 'Postres'";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;
	}

	public function Tips(){
		$sql="SELECT *
		FROM recetas, categorias, tipo
		WHERE categorias.id_categoria = tipo.id_categoria
		AND tipo.id_receta = recetas.id_receta
		AND categorias.cnombre LIKE 'Tips'";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;
	}

	public function Legumbres(){
		$sql="SELECT * FROM recetas, categorias, tipo
		WHERE recetas.id_receta=tipo.id_receta
		AND tipo.id_categoria=categorias.id_categoria
		AND	categorias.id_categoria = 2";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;
	}

	public function Pasta(){
		$sql="SELECT * FROM recetas, categorias, tipo
		WHERE recetas.id_receta=tipo.id_receta
		AND tipo.id_categoria=categorias.id_categoria
		AND	categorias.id_categoria = 3";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;
	}

	public function Patatas(){
		$sql="SELECT * FROM recetas, categorias, tipo
		WHERE recetas.id_receta=tipo.id_receta
		AND tipo.id_categoria=categorias.id_categoria
		AND	categorias.id_categoria = 4";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;
	}

	public function Verduras(){
		$sql="SELECT * FROM recetas, categorias, tipo
		WHERE recetas.id_receta=tipo.id_receta
		AND tipo.id_categoria=categorias.id_categoria
		AND	categorias.id_categoria = 5";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;
	}

	public function Arroz(){
		$sql="SELECT * FROM recetas, categorias, tipo
		WHERE recetas.id_receta=tipo.id_receta
		AND tipo.id_categoria=categorias.id_categoria
		AND	categorias.id_categoria = 15";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;
	}

	public function Aves(){
		$sql="SELECT * FROM recetas, categorias, tipo
		WHERE recetas.id_receta=tipo.id_receta
		AND tipo.id_categoria=categorias.id_categoria
		AND	categorias.id_categoria = 6";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;
	}

	public function Carnes(){
		$sql="SELECT * FROM recetas, categorias, tipo
		WHERE recetas.id_receta=tipo.id_receta
		AND tipo.id_categoria=categorias.id_categoria
		AND	categorias.id_categoria = 7";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;
	}

	public function Huevos(){
		$sql="SELECT * FROM recetas, categorias, tipo
		WHERE recetas.id_receta=tipo.id_receta
		AND tipo.id_categoria=categorias.id_categoria
		AND	categorias.id_categoria = 8";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;
	}

	public function Pescados(){
		$sql="SELECT * FROM recetas, categorias, tipo
		WHERE recetas.id_receta=tipo.id_receta
		AND tipo.id_categoria=categorias.id_categoria
		AND	categorias.id_categoria = 9";
		$consulta=$this->conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->recetas[]=new Receta($registro);
		}
		return $this->recetas;
	}

	public function nuevoElemento($nombre, $ingredientes, $descripcion, $url, $imagen){

		$sql="INSERT INTO recetas(nombre, ingredientes, descripcion, url, imagen) VALUES('$nombre', '$ingredientes', '$descripcion', '$url', '$imagen')";
		$consulta=$this->conexion->query($sql);
	}

	public function borrarElemento($id){

		$sql="DELETE FROM recetas WHERE id_receta=$id";
		$consulta=$this->conexion->query($sql);
	}

	public function guardarElemento($id, $nombre, $ingredientes, $descripcion, $url, $imagen){

		$sql="UPDATE recetas SET nombre='$nombre', ingredientes='$ingredientes', descripcion='$descripcion', url='$url', imagen='$imagen' WHERE id_receta=$id";
		$consulta=$this->conexion->query($sql);
	}


	// public function Global($cat){
	// 	switch ($cat) {
	// 		case 'aperitivos':
	// 			$sql="SELECT *
	// 			FROM recetas, categorias, tipo
	// 			WHERE categorias.id_categoria = tipo.id_categoria
	// 			AND tipo.id_receta = recetas.id_receta
	// 			AND categorias.cnombre LIKE 'Aperitivos'";
	// 			$consulta=$this->conexion->query($sql);
	// 			while($registro=$consulta->fetch_array()){
	// 				$this->recetas[]=new Receta($registro);
	// 			}
	// 			return $this->recetas;
	// 		break;
			
	// 		case 'Pplatos':
	// 			$sql="SELECT *
	// 			FROM recetas, categorias, tipo
	// 			WHERE categorias.id_categoria = tipo.id_categoria
	// 			AND tipo.id_receta = recetas.id_receta
	// 			AND categorias.cnombre LIKE 'Primeros platos'";
	// 			$consulta=$this->conexion->query($sql);
	// 			while($registro=$consulta->fetch_array()){
	// 				$this->recetas[]=new Receta($registro);
	// 			}
	// 			return $this->recetas;
	// 			break;
	// 	}
	// }

}

?>