<?php 

class Usuario{

	public $id_usuer;
	public $nombre;
	public $apellidos;
	public $login;
	public $password;
	public $correo;
	public $session;

	public function __construct($registro){

		$this->id_usuer=$registro['id_usuer'];
		$this->nombre=$registro['nombre'];
		$this->apellidos=$registro['apellidos'];
		$this->login=$registro['login'];
		$this->password=$registro['password'];
		$this->correo=$registro['correo'];
		$this->session=$registro['session'];
		$this->conexion=Conexion::conectar();
	}
}

?>