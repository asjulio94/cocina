<?php  

//Incluimos el modelo
require('Models/RecetaModel.php');
require('Models/RecetasModel.php');

//Llamo al modelo de datos de recetas, donde conectare con la BBDD mediante sentencias SQL
$misrecetas=new Recetas();

if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='index';
}

switch($accion){
	 case 'listado':

		$recetas=$misrecetas->listado(); //Me trae las recetas de la tabla

		//Establezco los datos que quiero mostrar en la vista
		$tituloWeb='Listado de recetas';
		$contenidoWeb='Una gran variedad';
		$nombre='receta';

		//Pasamos los datos a la vista y renderizamos
		echo $twig->render('RecetasView.html', Array('recetas'=>$recetas, 'titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'Receta'=>$nombre));
		break;

	case 'ver':
		$receta=$misrecetas->detalle($_GET['id']);

		// $receta=detallesalto($receta);

		//Pasar los datos a la vista y renderizar
		echo $twig->render('RecetaView.html', Array('receta'=>$receta));
		break;

	case 'index':

		//Mostramos la página que irá en el inicio
		echo $twig->render('inicioView.html');
		break;

	case 'Aperitivos':

		$recetas=$misrecetas->aperitivos(); //Me trae los aperitivos de la tabla

		//Recojemos los datos que queremos mostrar
		$tituloWeb='Aperitivos';
		$contenidoWeb='';
		$img='img/Recetas/Aperitivos/Portada/Portada.jpg';

		//Le damos los datos a la vista y se renderiza
		echo $twig->render('TipoView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas, 'img'=>$img));
		break;

	case 'Aperitivosv':

		$recetas=$misrecetas->aperitivos(); //Me trae los aperitivos de la tabla

		//Recojemos los datos que queremos mostrar
		$tituloWeb='Aperitivos';
		$contenidoWeb='';

		//Le damos los datos a la vista y se renderiza
		echo $twig->render('CategoriasView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas));
		break;

	case 'Pplatos':

		$recetas=$misrecetas->Pplatos(); //Me trae los primeros platos de la tabla

		//Recojemos los datos que queremos mostrar
		$tituloWeb='Primeros Platos - ';
		$contenidoWeb='Legumbres, pasta, patatas, verduras';
		$img='img/Recetas/1plato/1platos/Portada.jpg';

		//Le damos los datos a la vista y se renderiza
		echo $twig->render('TipoView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas, 'img'=>$img));
		break;

	case 'Pplatosv':

		$recetas=$misrecetas->Pplatos(); //Me trae los primeros platos de la tabla

		//Recojemos los datos que queremos mostrar
		$tituloWeb='Primeros Platos - ';
		$contenidoWeb='Legumbres, pasta, patatas, verduras';

		//Le damos los datos a la vista y se renderiza
		echo $twig->render('CategoriasView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas));
		break;

	case 'Legumbres':

		$recetas=$misrecetas->Legumbres(); //Recojo el tipo legumbres de las categorías

		//Datos personalizados
		$tituloWeb='Legumbres';
		$contenidoWeb='';
		$img='img/Recetas/1plato/Legumbres/Portada_de_legumbres.jpg';

		//Recojemos los datos para renderizarlos a una vista
		echo $twig->render('TipoView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas, 'img'=> $img));
		break;

	case 'Pasta':

		$recetas=$misrecetas->Pasta(); //Recojo el tipo pasta de las categorías

		//Datos personalizados
		$tituloWeb='Pasta';
		$contenidoWeb='';
		$img='img/Recetas/1plato/Pasta/Portada_pasta.jpg';

		//Recojemos los datos para renderizarlos a una vista
		echo $twig->render('TipoView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas, 'img'=> $img));
		break;

	case 'Patatas':

		$recetas=$misrecetas->Patatas(); //Recojo el tipo patatas de las categorías

		//Datos personalizados
		$tituloWeb='Patatas';
		$contenidoWeb='';
		$img='img/Recetas/1plato/Patatas/Portada_de_patatas.jpg';

		//Recojemos los datos para renderizarlos a una vista
		echo $twig->render('TipoView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas, 'img'=> $img));
		break;

	case 'Verdura':

		$recetas=$misrecetas->Verduras(); //Recojo el tipo verduras de las categorías

		//Datos personalizados
		$tituloWeb='Verduras';
		$contenidoWeb='';
		$img='img/Recetas/1plato/Verduras/Portada_verduras.jpg';

		//Recojemos los datos para renderizarlos a una vista
		echo $twig->render('TipoView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas, 'img'=>$img));
		break;

	case 'Arroz':

		$recetas=$misrecetas->Arroz(); //Recojo el tipo arroces de las categorías

		//Datos personalizados
		$tituloWeb='Arroz';
		$contenidoWeb='';
		$img='img/Recetas/1plato/Arroz/Portada_arroz.jpg';

		//Recojemos los datos para renderizarlos a una vista
		echo $twig->render('TipoView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas, 'img'=>$img));
		break;

	case 'Splatos':

		$recetas=$misrecetas->Splatos(); //Me trae los segundos platos de la tabla

		//Recojemos los datos que queremos mostrar
		$tituloWeb='Segundos Platos - ';
		$contenidoWeb='Aves, carnes, huevos, pescados';
		$img='img/Recetas/2plato/2platos/Portada.jpg';

		//Le damos los datos a la vista y se renderiza
		echo $twig->render('TipoView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas, 'img'=>$img));
		break;

	case 'Splatosv':

		$recetas=$misrecetas->Splatos(); //Me trae los segundos platos de la tabla

		//Recojemos los datos que queremos mostrar
		$tituloWeb='Segundos Platos - ';
		$contenidoWeb='Aves, carnes, huevos, pescados';

		//Le damos los datos a la vista y se renderiza
		echo $twig->render('CategoriasView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas));
		break;

	case 'Aves':

		$recetas=$misrecetas->Aves(); //Recojo el tipo aves de las categorías

		//Datos personalizados
		$tituloWeb='Aves';
		$contenidoWeb='';
		$img='img/Recetas/2plato/Aves/Portada_aves.jpg';

		//Recojemos los datos para renderizarlos a una vista
		echo $twig->render('TipoView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas, 'img'=>$img));
		break;

	case 'Carnes':

		$recetas=$misrecetas->Carnes(); //Recojo el tipo carnes de las categorías

		//Datos personalizados
		$tituloWeb='Carnes';
		$contenidoWeb='';
		$img='img/Recetas/2plato/Carnes/Portada_carnes.jpg';

		//Recojemos los datos para renderizarlos a una vista
		echo $twig->render('TipoView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas, 'img'=>$img));
		break;

	case 'Huevos':

		$recetas=$misrecetas->Huevos(); //Recojo el tipo huevos de las categorías

		//Datos personalizados
		$tituloWeb='Huevos';
		$contenidoWeb='';
		$img='img/Recetas/2plato/Huevos/Portada_huevos.jpg';

		//Recojemos los datos para renderizarlos a una vista
		echo $twig->render('TipoView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas, 'img'=>$img));
		break;

	case 'Pescados':

		$recetas=$misrecetas->Pescados(); //Recojo el tipo pescados de las categorías

		//Datos personalizados
		$tituloWeb='Pescados';
		$contenidoWeb='';
		$img='img/Recetas/2plato/Pescados/Portada_pescados.jpg';

		//Recojemos los datos para renderizarlos a una vista
		echo $twig->render('TipoView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas, 'img'=>$img));
		break;

	case 'Salsas':

		$recetas=$misrecetas->Salsas(); //Me trae las salsas de las recetas

		//Recojemos los datos que queremos mostrar
		$tituloWeb='Salsas';
		$contenidoWeb='';
		$img='img/Recetas/Salsas/Portada.jpg';

		//Le damos los datos a la vista y se renderiza
		echo $twig->render('TipoView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas, 'img'=>$img));
		break;

	case 'Salsasv':

		$recetas=$misrecetas->Salsas(); //Me trae las salsas de las recetas

		//Recojemos los datos que queremos mostrar
		$tituloWeb='Salsas';
		$contenidoWeb='';

		//Le damos los datos a la vista y se renderiza
		echo $twig->render('CategoriasView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas));
		break;

	case 'Masas':

		$recetas=$misrecetas->Masas(); //Me trae las masas de la tabla

		//Recojemos los datos que queremos mostrar
		$tituloWeb='Masas';
		$contenidoWeb='';
		$img='img/Recetas/Masas/Portada.jpg';

		//Le damos los datos a la vista y se renderiza
		echo $twig->render('TipoView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas, 'img'=>$img));
		break;

	case 'Masasv':

		$recetas=$misrecetas->Masas(); //Me trae las masas de la tabla

		//Recojemos los datos que queremos mostrar
		$tituloWeb='Masas';
		$contenidoWeb='';

		//Le damos los datos a la vista y se renderiza
		echo $twig->render('CategoriasView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas));
		break;

	case 'Postres':

		$recetas=$misrecetas->Postres(); //Me traigo los postres de la tabla

		//Recojemos los datos que queremos mostrar
		$tituloWeb='Postres';
		$contenidoWeb='';
		$img='img/Recetas/Postres/Portada.jpg';

		//Le damos los datos a la vista y se renderiza
		echo $twig->render('TipoView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas, 'img'=>$img));
		break;

	case 'Postresv':

		$recetas=$misrecetas->Postres(); //Me traigo los postres de la tabla

		//Recojemos los datos que queremos mostrar
		$tituloWeb='Postres';
		$contenidoWeb='';

		//Le damos los datos a la vista y se renderiza
		echo $twig->render('CategoriasView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas));
		break;

	case 'Tips':

		$recetas=$misrecetas->Tips(); //Recojo los consejos de la tabla

		//Recojemos los datos que queremos mostrar
		$tituloWeb='Tips - ';
		$contenidoWeb='¿Cómo se hace?';

		//Le damos los datos a la vista y se renderiza
		echo $twig->render('CategoriasView.html', Array('titulo'=>$tituloWeb, 'contenido'=>$contenidoWeb, 'receta'=>$recetas));
		break;

	// case 'Global':

	// 	$recetas=$misrecetas->Global($_GET['cat']); //Me trae los datos de la tabla

	// 	//Recojemos los datos que queremos mostrar
	// 	$tituloWeb=$_GET['cat'];

	// 	//Le damos los datos a la vista y se renderiza
	// 	echo $twig->render('GlobalView.html', Array('titulo'=>$tituloWeb, 'Global'=>$recetas));
	// 	break;

	case 'Presentacion':

		//Recojemos la imagen y el texto
		$img='img/QuienSoy/Contacto.jpg';
		$contenidoWeb1='Me llamo Ana Robres, soy natural de Alcañiz, ciudad conocida por los aficionados al motor (MotorLand). Resido en Zaragoza junto con mi marido Julio y mis dos hijos.';

		$contenidoWeb2='Si tuviese que definir mi cocina, diría que es generacional, ha pasado de mi abuela a mi madre y de mi madre a mí. Lógicamente  hay platos totalmente tradicionales y otros más novedosos por el constante aprendizaje y experimentación.';

		$contenidoWeb3='Me encanta aprender y el motivo de estos vídeos no es el de enseñar,  pero sí el de guiar a esos jóvenes que tiene poca experiencia en este  apasionante mundo.';

		$contenidoWeb4='La idea de hacer estos vídeos parte de la necesidad de enseñar a mis hijos. En un principio pensé en escribir un libro, pero dado que lo que más les agrada son las nuevas tecnologías, me decidí por crear esta web y grabar mi recetario.';

      	$contenidoWeb5='Agradeceros las muestras de cariño que estais teniendo conmigo, nunca imaginé que algo tan sencillo como mi cocina, esté teniendo tanta aceptación.';

		//Renderizamos la página que se verá en Quien Soy
		echo $twig->render('PresentacionView.html', Array('img'=>$img, 'contenido1'=>$contenidoWeb1, 'contenido2'=>$contenidoWeb2, 'contenido3'=>$contenidoWeb3, 'contenido4'=>$contenidoWeb4, 'contenido5'=>$contenidoWeb5));
		break;

		case 'insercion':

		$nombre=$_POST['nombre'];
		$ingredientes=$_POST['ingredientes'];
		$descripcion=$_POST['descripcion'];
		$url=$_POST['url'];
		$imagen=$_POST['imagen'];

		$misrecetas->nuevoElemento($nombre, $ingredientes, $descripcion, $url, $imagen);
		header('location: index.php?contr=RecetaController.php&accion=listado');
		break;

		case 'Update':

		$receta=$misrecetas->detalle($_GET['id']);
		echo $twig->render('FormularioView.html', Array('receta'=>$receta, 'accion'=>'modificacion'));
		break;

		case 'modificacion':

		$id=$_POST['id'];
		$nombre=$_POST['nombre'];
		$ingredientes=$_POST['ingredientes'];
		$descripcion=$_POST['descripcion'];
		$url=$_POST['url'];
		$imagen=$_POST['imagen'];

		$misrecetas->guardarElemento($id, $nombre, $ingredientes, $descripcion, $url, $imagen);
		header('location: index.php?contr=RecetaController.php&accion=listado');
		break;

		case 'Delete':

		$receta=$misrecetas->borrarElemento($_GET['id']);
		header('location: index.php?contr=RecetaController.php&accion=listado');
		break;

}



?>