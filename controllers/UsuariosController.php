<?php

//Incluimos el modelo
require('Models/UsuarioModel.php');
require('Models/UsuariosModel.php');

//Añadimos sino están aún los modelos para hacer un CRUD si eres superusuario
require_once('Models/RecetaModel.php');
require_once('Models/RecetasModel.php');

//Llamo al modelo de datos de usuarios, donde conectaré con la BBDD mediante sentencias SQL
$misusuarios=new Usuarios();

//Recogemos la accion que queremos realizar con isset($_GET['accion'])
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='index';
}

switch($accion){
	 case 'listado':

		$usuarios=$misusuarios->listado(); //Me trae las usuarios de la tabla

		//Establezco los datos que quiero mostrar en la vista
		$tituloWeb='Usuarios registrados';

		//Pasamos los datos a la vista y renderizamos
		echo $twig->render('UsuariosView.html', Array('recetas'=>$recetas, 'titulo'=>$tituloWeb));
		break;

	case 'ver':

		$usuarios=$misusuarios->detalle($_GET['id']);

		//Pasar los datos a la vista y renderizar
		echo $twig->render('RecetaView.html', Array('receta'=>$receta));
		break;

	case 'index':

		//Mostramos la página que irá en el inicio
		echo $twig->render('inicioView.html');
		break;

	case 'Contacto':

		$img='img/Contactar/form.jpg';

		echo $twig->render('ContactoView.html', Array('img'=>$img));
		break;

	case 'Create':

		echo $twig->render('FormularioView.html', Array('accion'=>'insercion'));
		break;

	case 'Registro':

		$tituloWeb='Registro';

		echo $twig->render('envio.html', Array('titulo'=>$tituloWeb));
		break;

	case 'SubRegistro':

		$tituloWeb='Registro';
		$error_mensaje='';

		//Recojo los datos del envío POST
			$nombre=$_POST['nombre'];
			$apellidos=$_POST['apellidos'];
			$correo=$_POST['correo'];
			$login=$_POST['login'];
			$password=$_POST['password'];
			$pass=$_POST['password2'];

		//Valido los datos enviados
			if(
				strlen($nombre) >= 2 &&
				strpos($correo, '@') > 0 &&
				(strlen($password) >= 8 && strlen($password) <= 16) && 
				strlen($login) >= 4
			){
				if ($password===$pass) {
					require_once('includes/config.php');
					require_once('includes/conexion.php');

					$conexion=Conexion::conectar();
					$password=password_hash($password, PASSWORD_DEFAULT);
					$correo=$correo;
					$sql="INSERT INTO usuarios VALUES ('', '$nombre', '$apellidos', '$login', '$password', '$correo', '')";
		  			$consulta=$conexion->query($sql);
		  			header('location: index.php?contr=RecetaController.php');
				}
					else {
						$error_mensaje='Las contraseñas no coinciden.';
					}
			}
				else{
					$error_mensaje='ERROR: Por favor, rellene todos los campos requeridos.';
			}

		echo $twig->render('envio.html', Array('titulo'=>$tituloWeb, 'ERROR'=>$error_mensaje));
		break;

}

?>