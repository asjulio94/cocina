<?php  

//Incluimos el modelo
require('Models/RecetaModel.php');
require('Models/RecetasModel.php');

//Llamo al modelo de datos de recetas, donde conectare con la BBDD mediante sentencias SQL
$misrecetas=new Recetas();

if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='Ensaladas';
}

switch($accion){
	 case 'Ensaladas':

		$ensaladas=$misrecetas->VegetarianasE(); //Me trae las recetas de la tabla clasificadas por el tipo ensaladas

		//Establezco los datos que quiero mostrar en la vista
		$tituloWeb='Ensaladas';

		//Pasamos los datos a la vista y renderizamos
		echo $twig->render('RecetasView.html', Array('recetas'=>$ensaladas, 'titulo'=>$tituloWeb));
		break;

	case 'Aperitivos':

		$aperitivos=$misrecetas->VegetarianasA();

		$tituloWeb='Aperitivos';

		echo $twig->render('RecetasView.html', Array('recetas'=>$aperitivos, 'titulo'=>$tituloWeb));

	case 'Verduras':

		$verduras=$misrecetas->VegetarianasV();

		$tituloWeb='Verduras';

		echo $twig->render('VegetarianasView.html', Array('recetas'=>$verduras, 'titulo'=>$tituloWeb));

	case 'Salsas':

		$salsas=$misrecetas->VegetarianasS();

		$tituloWeb='Salsas';

		echo $twig->render('RecetasView.html', Array('recetas'=>$salsas, 'titulo'=>$tituloWeb));

	case 'Pasta':

		$pasta=$misrecetas->VegetarianasP();

		$tituloWeb='Pasta';

		echo $twig->render('RecetasView.html', Array('recetas'=>$pasta, 'titulo'=>$tituloWeb));

	case 'Legumbres':

		$legumbre=$misrecetas->VegetarianasL();

		$tituloWeb='Legumbres';

		echo $twig->render('RecetasView.html', Array('recetas'=>$legumbre, 'titulo'=>$tituloWeb));
}
?>