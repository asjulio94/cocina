<?php
class Conexion{

	static public $host=DB_HOST;
	static public $usuario=DB_USUARIO;
	static public $clave=DB_CLAVE;
	static public $base=DB_BASE;

	public static function conectar(){
		$conexion=new Mysqli(self::$host, self::$usuario, self::$clave, self::$base);
		$conexion->set_charset('utf8');
		return $conexion;
	}

}
?>