<?php

// FUNCIONALIDAD 1
//Si NO EXISTE LA VARIABLE de SESSION, LA CREO poo inicialmente

if(!isset($_SESSION['sesion'])){

  //Si tengo cokkie
  if(isset($_COOKIE['conectado'])){
    //Recojo el valor de la cookie
    $codigo=$_COOKIE['conectado'];
    //Hago una consulta a la bbdd
    $sql="SELECT id_user AS id, login, nombre, password, correo FROM usuarios WHERE session='".$codigo."'";

    //Si existe un usuario con ese valor, lo recojo y si no... la variable de SESSION sera NULL
    $conexion=Conexion::conectar();
    $consulta=$conexion->query($sql);
    if($registro = $consulta->fetch_assoc()){
      $_SESSION['sesion'] = [
          'id'      => $registro['id'],
          'nombre'  => $registro['nombre'],
          'login'   => $registro['login'],
          'estado'  => true,
          'correo'  => $registro['correo'] 
        ];
      }else{
      $_SESSION['sesion']=false;
    }
  }//Fin de if($_COOKIE)
  else{
   $_SESSION['sesion']=false;
  }
}//Fin de if($_SESSION)

$sesion=$_SESSION['sesion'];

// FUNCIONALIDAD 2
//Si el usuario ha pulsado el enlace desconectar, cierro session
if(isset($_GET['desconectar'])){
  $_SESSION['sesion']=false;
  setcookie('conectado', false, 0); //EXPIRO LA COOKIE
}

// FUNCIONALIDAD 3
//Si pulso el boton entrar, compruebo el correo y la clave
if(isset($_POST['entrar'])){

	//recojo correo y clave
  $login=$_POST['login'];
	$clave=$_POST['clave'];

	//Compruebo si son correctos
  $conexion=Conexion::conectar();
	$sql="SELECT id_user AS id, login, nombre, password, correo FROM usuarios WHERE login='".$login."'";
 
    $consulta=$conexion->query($sql);
    if($registro = $consulta->fetch_assoc()){

    //Comparamos el correo del login con el correo del hash
      if(password_verify($clave, $registro['password'])){
        //Pregunto si quiero guardar la cookie
        $_SESSION['sesion'] = [
          'id'      => $registro['id'],
          'nombre'  => $registro['nombre'],
          'login'   => $registro['login'],
          'estado'  => true,
          'correo'  => $registro['correo']
        ];

        if(isset($_POST['recordar'])){
          //Me creo un codigo UNICO para este usuario, y lo guardo en la tabla de usuarios, y en la cookie
          $codigo=md5(time()+rand(100, 999999999));
          $sql="UPDATE usuarios SET session='".$codigo."' WHERE id_user='".$registro['id']."'";
          $conexion=Conexion::conectar();
          $consulta=$conexion->query($sql);
          setcookie('conectado', $codigo, time()+60*60*24*7);
        } //Fin del si guardar una cookie
      }
      else{
          header("location:index.php");
      }

    } //Fin del $registro assoc
    
} //Fin del if, de comprobar la pulsacion de ENTRAR

?>

