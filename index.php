<?php
session_start();
//Traemos los archivos de conexion de la BBDD
require('includes/config.php');
require('includes/conexion.php');
require('includes/functions.php');
require('includes/login.php');

//Añadimos nuestro autoload de vendor
require('vendor/autoload.php');

//Le decimos donde van a ir las plantillas de twig
$loader = new Twig_Loader_Filesystem('views/');

//ESTO PARA DESARROLLO
$twig = new Twig_Environment($loader);

// //ESTO PARA PRODUCCION
// $twig = new Twig_Environment($loader, array('cache' => 'cache/'));

$twig->addGlobal('session', $_SESSION);

//Recojo el controlador que quiero llamar
if(isset($_GET['contr'])){
	$contr=$_GET['contr'];
}else{
	$contr='RecetaController.php';
}

//Agregamos la variable global a twig
$twig->addGlobal('contr', $contr);

//Tengo que LLAMAR al controlador que quiero hacer funcionar
require('controllers/'.$contr);
?>